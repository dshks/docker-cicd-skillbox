FROM python:3-alpine
ENV PYTHONUNBUFFERED 1
WORKDIR /code
COPY . /code
RUN mkdir /db
RUN /usr/local/bin/python -m pip install --upgrade pip && pip install -r requirements.txt
EXPOSE 8000
CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8000
